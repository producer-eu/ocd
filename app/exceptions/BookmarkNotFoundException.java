package exceptions;

public class BookmarkNotFoundException extends Exception {
    public BookmarkNotFoundException(String s) {
        super(s);
    }
}
