package exceptions;

public class ClaimValidationJWTException extends Exception {

    public ClaimValidationJWTException(String message){
        super(message);
    }
}
