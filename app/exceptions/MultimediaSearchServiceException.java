package exceptions;

public class MultimediaSearchServiceException extends Exception {
    public MultimediaSearchServiceException(String s) {
        super(s);
    }
}
