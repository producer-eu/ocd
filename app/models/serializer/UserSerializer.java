package models.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.User;

import java.io.IOException;

/**
 * Created by Pasquale on 12/07/2017.
 */
public class UserSerializer extends StdSerializer<User> {

    public UserSerializer() {
        this(null);
    }

    public UserSerializer(Class<User> t) {
        super(t);
    }

    @Override
    public void serialize(
            User value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeStartObject();
        jgen.writeStringField("id", value.getId().toHexString());
        jgen.writeStringField("username", value.getUsername());
        jgen.writeStringField("firstName", value.getFirstName());
        jgen.writeStringField("lastName", value.getLastName());
        jgen.writeStringField("email", value.getEmail());
        jgen.writeStringField("token", value.getToken());
        //jgen.writeStringField("role", value.getRole().name());
        if(value.getRoles()!=null)   jgen.writeObjectField("roles", value.getRoles());
        if(value.getRoles()!=null)   jgen.writeObjectField("privileges", value.getPermissions());
        jgen.writeStringField("access_token", value.getAccess_token());
        jgen.writeEndObject();
    }
}
