package services.semantic;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import gate.*;
import gate.creole.ExecutionException;
import gate.util.persistence.PersistenceManager;
import play.Play;

import java.io.File;
import java.net.URL;
import java.util.*;

/**
 * This class implements a semantic analyzer tool that uses the GATE Embedded library to analyze text files and extract
 * meaningful Annotations. The original GATE application used is the ANNIE default application, but the application
 * itself can be modified with the GATE software and reloaded as necessary. Furthermore, the semantic analyzer is
 * compatible with URLs, and is able to analyse the HTM content of web-pages.
 *
 * Update: The ANNIE application used is defined in the "ANNIE_with_extras.gapp" file, and has some additional features like
 * basic animals annotation capabilities.
 *
 * The Class is designed as a Singleton since there needs to be only one initialized Semantic Analyzer.
 */
public class SemanticAnalyzer {

    /** Singleton instance */
    private static SemanticAnalyzer semanticAnalyzer = null;

    /** List of annotation types to write out. */
    private List<String> annotationsTypesToWrite = null;

    /** Encoding of the input file */
    private String encoding = null;

    /** GATE application used to process documents */
    private CorpusController application;

    /** Performance monitoring timestamps */
    private long t1;
    private long t2;
    private long t3;

    /**
     *  Singleton constructor.
     *  Initialize a semantic tool with the default application.
     */
    private SemanticAnalyzer() throws Exception {

        // performance timestamps variables
        double t;
        long t0  = System.currentTimeMillis();

        // retrieve GATE home and GATE plugins
        if(Gate.getGateHome() == null)
            Gate.setGateHome(Play.application().getFile("conf/semantic"));
        if(Gate.getPluginsHome() == null)
            Gate.setPluginsHome(Play.application().getFile("conf/semantic/plugins"));

        // initialize GATE
        Gate.init();

        // load the application from the default plugins
        File pluginsHome = Gate.getPluginsHome();
        File anniePlugin = new File(pluginsHome,"ANNIE");
        File applicationFile = new File(anniePlugin,"ANNIE_with_extras.gapp");

        // default annotations to write out
        annotationsTypesToWrite = new ArrayList<>();
        annotationsTypesToWrite.add("Person");
        annotationsTypesToWrite.add("Date");
        annotationsTypesToWrite.add("Location");
        annotationsTypesToWrite.add("Animal");
        annotationsTypesToWrite.add("Organization");

        // load the saved GATE application
        application = (CorpusController) PersistenceManager.loadObjectFromFile(applicationFile);

        t1 = System.currentTimeMillis();
        t = ((double) (t1 - t0))/1000;
        System.out.println("SemanticAnalyser: " + t + "s to initialize GATE application");

    }

    /**
     * Singleton getter.
     * @return the Semantic Analyzer instance
     */
    public static synchronized SemanticAnalyzer getSemanticAnalyzer() {
        if (semanticAnalyzer == null) {
            try {
                semanticAnalyzer = new SemanticAnalyzer();
            } catch (Exception e) {
                System.out.println("Failed initializing Semantic Analyzer singleton: " + e);
            }
        }
        return semanticAnalyzer;
    }

    /**
     * Returns a string containing the GATE annotations generated by a GATE application (default is ANNIE).
     *
     * @param file the text file that should be analysed
     * @return the string in JSON format with the annotation categories as fields keys and arrays of annotations
     *         as fields content
     */
    public String getFileAnnotations(File file) throws Exception {

        /* General procedures for annotation extraction:
         * 1. Load application
         * 2. Create corpus and add doc
         * 3. Run application
         * 4. Process result
         */

        // performance timestamps variables
        double t;
        t1 = System.currentTimeMillis();

        // Create a Corpus to be processed by the application
        Corpus corpus = Factory.newCorpus("BatchProcessApp Corpus");
        application.setCorpus(corpus);

        // load the document (using the specified encoding if one was given)
        Document doc = Factory.newDocument(file.toURI().toURL(), encoding);

        // put the document in the corpus
        corpus.add(doc);


        t2 = System.currentTimeMillis();
        t = ((double) (t2 - t1)) / 1000;
        System.out.println("SemanticAnalyser: " + t + "s to create corpus and add doc");

        // run the application
        try {
            application.execute();
        } catch (ExecutionException e) {
            throw throwAsError("Your document could not be processed; usually that means that no words could " +
                    "be found or that your file is empty.");
        }

        t3 = System.currentTimeMillis();
        t = ((double) (t3 - t2)) / 1000;
        System.out.println("SemanticAnalyser: " + t + "s to run GATE application");

        // remove the document from the corpus again
        corpus.clear();

        return processResult(doc);

    }

    /**
     * Returns a string containing the GATE annotations generated by a GATE application (default is ANNIE).
     *
     * @param url the link of the web-page to analyse
     * @return the string in JSON format with the annotation categories as fields keys and arrays of annotations
     *         as fields content
     */
    public String getUrlAnnotations(String url) throws Exception {

        // performance timestamps variables
        double t;
        t1 = System.currentTimeMillis();

        // Create a Corpus to be processed by the application
        Corpus corpus = Factory.newCorpus("BatchProcessApp Corpus");
        application.setCorpus(corpus);
        FeatureMap params = Factory.newFeatureMap();

        // get the doc from the URL
        URL u;
        Document doc;
        try {
            u = new URL(url);
            params.put("sourceUrl", u);
            doc = (Document) Factory.createResource("gate.corpora.DocumentImpl", params);
        } catch (NullPointerException e) {
            throw  throwAsError("The URL you submitted cannot be reached.");
        } catch (Exception e) {
            if (url.equals(""))
                throw throwAsError("The URL you submitted is empty");
            else
                throw throwAsError("\""+ url +"\" is not an URL...");
        }

        //put the document in the corpus
        corpus.add(doc);

        t2 = System.currentTimeMillis();
        t = ((double) (t2 - t1)) / 1000;
        System.out.println("SemanticAnalyser: " + t + "s to create corpus and add doc");

        // run the application
        try {
            application.execute();
        } catch (ExecutionException e) {
            throw throwAsError("Your document could not be processed; usually that means that no words could " +
                    "be found or that your file is empty.");
        }

        t3 = System.currentTimeMillis();
        t = ((double) (t3 - t2)) / 1000;
        System.out.println("SemanticAnalyser: " + t + "s to run GATE application");

        // remove the document from the corpus again
        corpus.clear();

        return processResult(doc);

    }

    /**
     * Retrieve the GATE annotations from the document, extract the necessary information and create a JSON object that
     * will consist in the final output of the API.
     *
     * @param doc the processed GATE document from which the annotations will be extracted
     * @return the final JSON string to be exposed, containing only annotations content and occurrences
     */
    private String processResult(Document doc) throws Exception {

        // use Jackson to create the JSON representation of the annotations
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonRoot = mapper.createObjectNode();

        // Create a temporary Set to hold the annotations we wish to write out
        Set<Annotation> annotationSet = new HashSet<>();

        // get all annotations from the document
        AnnotationSet defaultAnnotations = doc.getAnnotations();
        for (String annotationType : annotationsTypesToWrite) {
            // extract all the annotations of each requested type and add them to the temporary set
            AnnotationSet annotationsOfThisType = defaultAnnotations.get(annotationType);
            if(annotationsOfThisType != null) {
                jsonRoot.set(annotationType, mapper.createArrayNode());
                annotationSet.addAll(annotationsOfThisType);
            }
        }

        // Release the document, as it is no longer needed; we only work with the retrieved annotations from now on
        Factory.deleteResource(doc);

        // extract the necessary information from the annotations and create the JSON object
        ArrayList<Integer> matched = new ArrayList<>(); // list to prevent duplicate annotations

        // Map of annotations IDs, content and type
        HashMap<Integer, String> idTypeMap = new HashMap<>();
        HashMap<Integer, String> idContentMap = new HashMap<>();
        HashMap<Integer, Integer> idOccurrenceMap = new HashMap<>();

        for (Annotation annotation : annotationSet) {

            // annotation features
            FeatureMap annotationFeatures = annotation.getFeatures();

            // annotation content
            String annotationContent = doc.getContent().getContent(annotation.getStartNode().getOffset(),
                    annotation.getEndNode().getOffset()).toString();

            // get list of annotation matches
            @SuppressWarnings("unchecked") // "matches" field is stored as list of integers by GATE
            List<Integer> matchesList = (List<Integer>) annotationFeatures.get("matches");

            // annotation occurrences
            int annotationOccurrences = (matchesList != null) ? matchesList.size() : 1;

            if (matchesList != null) {
                // annotation has other matches: compare it to the expected matches already seen
                if (matched.contains(annotation.getId())) {
                    // annotation was matched by another one: find that annotation
                    for (Integer key : idContentMap.keySet()) {
                        if (matchesList.contains(key)) {
                            // found the annotation: compare contents
                            if (annotationContent.length() > idContentMap.get(key).length()) {
                                // annotation content is longer: update content hash map
                                idContentMap.put(key, annotationContent);
                            } // content is shorter
                        } // that key is not in the annotation matches
                    }
                } else {
                    // annotation is the first of its kind, update all data structures
                    idContentMap.put(annotation.getId(), annotationContent);
                    idOccurrenceMap.put(annotation.getId(), annotationOccurrences);
                    idTypeMap.put(annotation.getId(), annotation.getType());
                    matched.addAll(matchesList);
                }
            } else {
                // annotation is single: add it directly to the JSON object
                this.addAnnotationToJson(jsonRoot, annotation.getType(), annotationContent, annotationOccurrences,
                        mapper);
            }

        }

        // add all the remaining multiple-occurrence annotations to the JSON
        for (int id : idTypeMap.keySet()) {
            String type = idTypeMap.get(id);
            String content = idContentMap.get(id);
            int occurrences = idOccurrenceMap.get(id);
            addAnnotationToJson(jsonRoot, type, content, occurrences, mapper);
        }

        String res = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonRoot);

        // performance timestamps variables
        long tEnd = System.currentTimeMillis();
        double t = ((double) (tEnd - t3)) / 1000;
        System.out.println("SemanticAnalyser: " + t + "s to process result\n");
        t = ((double) (tEnd - t1))/1000;
        System.out.println("SemanticAnalyser: " + t + "s total time\n");

        // pretty print the ugly json
        return res;

    }


    /**
     * Create a JSON node from the annotation information and insert it into the JSON tree.
     *
     * @param root the JSON object into which the node will be inserted
     * @param type the annotation type, used for getting the appropriate JSON array under the root
     * @param content the annotation content
     * @param occurrences how many matches the annotation had in the original document
     * @param mapper the JSON mapper used to construct the JSON node
     */
    private void addAnnotationToJson(ObjectNode root, String type, String content, int occurrences,
                                     ObjectMapper mapper) {

        // create the JSON node
        ObjectNode jsonAnnotation = mapper.createObjectNode();

        // set annotation content
        jsonAnnotation.put("content", content);

        // set annotation occurrence
        jsonAnnotation.put("occurrences", occurrences);

        // add annotation to the appropriate list of the JSON root
        ArrayNode jsonAnnotationsArray  = ((ArrayNode) root.get(type));
        this.insertNodeByOccurrences(jsonAnnotationsArray, jsonAnnotation, occurrences);

    }

    /**
     * Insert the Jackson node annotation inside the array of annotations keeping them ordered by occurrence.
     *
     * The algorithm runs in linear time: iterates through the array until the occurrence value of the next node is
     * lower than the new node's value.
     *
     * @param array the Jackson ArrayNode to insert the node into
     * @param element the Jackson ObjectNode to insert into the array
     */
    private void insertNodeByOccurrences(ArrayNode array, ObjectNode element, int occurrences) {

        if (array.size() != 0) {
            for (int i = 0; i < array.size(); i++) {
                int otherOccurrences = array.get(i).get("occurrences").asInt();
                if (otherOccurrences <= occurrences) {
                    array.insert(i, element);
                    break;
                }
                if (i == array.size() - 1) array.add(element);
            }
        } else array.add(element);

    }


    /**
     * Handle semantic analyzer exceptions.
     *
     * @param message the error type
     * @return String to expose
     */
    private Exception throwAsError(String message) {

        return new Exception(message);

    }

}