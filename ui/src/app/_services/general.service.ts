import { Injectable } from "@angular/core"
import { Http, Headers, RequestOptions, Response } from "@angular/http"

@Injectable()
export class GeneralService {

    constructor(private http: Http) { }

    get(url: string) {
        return this.http.get(url)
            .map((response: Response) => response.json());
    }

    checkurl(url: string) {
        return this.http.get(url);
    }

}
