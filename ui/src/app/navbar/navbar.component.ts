import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Http } from '@angular/http';
import { AuthService } from '../_services/index';


@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

    profile: any;

    constructor(public http: Http, public auth: AuthService, private changeDetector: ChangeDetectorRef) {
    }

    ngOnInit() {
    }

    ngAfterViewChecked(){
        this.changeDetector.detectChanges();
      }

    getPicture() {

        if (this.profile && this.profile.picture) {
            return this.profile.picture;
        } else {
            return '/assets/images/account.png';
        }

    }

    isAdmin() {
            // console.log('IS ADMIN', this.profile);
            return this.profile && this.profile.nickname && this.profile.nickname === 'johnnyproducer2017';
    }


    isLoggedIn() {
        if (this.auth.isAuthenticated()) {
            // console.log('nav bar authenticated');
            return true;
        }
        else {
            // console.log('nav bar not authenticated');
            return false;
        }
    }

    getLoggedUser() {
        this.profile = JSON.parse(localStorage.getItem('profile'));
        if (this.profile && this.profile.nickname) {
            return this.profile.nickname;
        }
    }

    logout() {
        this.auth.logout();
    }

}
